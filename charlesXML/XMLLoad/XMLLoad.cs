﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace LibCompareXML
{
    public class XMLExtract
    {
        private string _caminhoXmlResultado=null;

        public XMLExtract(string XMLResultado)
        {
           _caminhoXmlResultado = XMLResultado;
        }

        /// <summary>
        /// Carrega o XML de Comparação gerado pela ferramenta.[MDCXML.exe]
        /// Usando as seguintes configurações no arquivo Options.(InsertPI = 1,InsertTags = 1,PrintResultToOutputFile=1)
        /// </summary>
        /// <returns></returns>
        public List<XMLItens> LoadCompare()
        {
            List<XMLItens> lstItens = new List<XMLItens>();

            try
            {
                XmlTextReader reader = new XmlTextReader(_caminhoXmlResultado);
                ArrayList elementos = new ArrayList();

                string tagName=null, tipoTagStart=null, newValue=null, oldValue=null, lastOperation = null;             

                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:

                            if (lastOperation != null && lastOperation.Equals("comment"))
                            {
                                if (reader.HasAttributes)
                                {
                                    while (reader.MoveToNextAttribute())
                                        newValue += string.Format("{0}=\"{1}\" ", reader.Name, reader.Value);
                                }
                                else
                                    newValue = reader.Value;

                                XMLItens novosItens = new XMLItens()
                                {
                                    operation = tipoTagStart,
                                    newValue = newValue.TrimStart().TrimEnd(),
                                    tag = tagName,
                                    oldValue = oldValue
                                };
                                lstItens.Add(novosItens);

                                //Reincia variaveis.
                                tagName = null;
                                tipoTagStart = null;
                                newValue = null;
                                oldValue = null;

                                lastOperation = null;
                            }

                            break;

                        case XmlNodeType.Comment:

                            if (reader.Value.TrimStart().StartsWith("MDCXML has compared two XML files"))
                                continue;

                            tipoTagStart = "start_value_changed";
                            lastOperation = "comment";

                            oldValue = reader.Value.Replace("\r\n", "").TrimStart().TrimEnd();
                            break;

                        case XmlNodeType.Text:
                            if (lastOperation != null && lastOperation.Equals("end_value_changed"))
                            {

                                oldValue = reader.Value.Replace("\r\n", "");

                                XMLItens novosItens = new XMLItens()
                                {
                                    operation = tipoTagStart,
                                    newValue = newValue,
                                    tag = tagName,
                                    oldValue = oldValue
                                };
                                lstItens.Add(novosItens);

                                //Reincia variaveis.
                                tagName = null;
                                tipoTagStart = null;
                                newValue = null;
                                oldValue = null;
                                lastOperation = null;
                                break;
                            }
                            break;

                        case XmlNodeType.ProcessingInstruction:
                            if (reader.Name.Equals("start_value_changed"))
                            {
                                tipoTagStart = reader.Name;

                                reader.MoveToContent();//Move o leitor de volta para o elemento.
                                newValue = reader.Value.Replace("\r\n", "");
                                break;
                            }
                            if (reader.Name.Equals("end_value_changed"))
                            {
                                lastOperation = reader.Name;
                                break;
                            }
                            if (reader.Name.Equals("start_elements_added"))
                            {
                                tipoTagStart = reader.Name;

                                reader.MoveToContent();
                                tagName = reader.Name;

                                if (reader.HasAttributes)
                                {
                                    //for(int i=0; i< reader.AttributeCount; i++)
                                    //{
                                    //    newValue = reader.GetAttribute(i);
                                    //}                                    
                                    while (reader.MoveToNextAttribute())
                                        newValue += string.Format("{0}=\"{1}\" ", reader.Name, reader.Value);
                                }
                                else
                                    newValue = reader.ReadInnerXml().Replace("\r\n", "");
                                break;
                            }
                            if (reader.Name.Equals("start_elements_deleted"))
                            {
                                tipoTagStart = reader.Name;

                                reader.MoveToContent();
                                tagName = reader.Name;
                                oldValue = reader.ReadInnerXml().Replace("\r\n", "");

                                XMLItens novosItens = new XMLItens()
                                {
                                    operation = tipoTagStart,
                                    tag = tagName,
                                    oldValue = oldValue
                                };
                                lstItens.Add(novosItens);

                                break;
                            }
                            if (reader.Name.Equals("end_elements_added"))
                            {
                                XMLItens novosItens = new XMLItens()
                                {
                                    operation = tipoTagStart,
                                    newValue = newValue,
                                    tag = tagName
                                };
                                lstItens.Add(novosItens);

                                //Reincia variaveis.
                                tagName = null;
                                tipoTagStart = null;
                                newValue = null;
                                oldValue = null;

                                break;
                            }
                            break;
                    }
                }

//                return lstItens;
            }
            catch (Exception erro)
            {
                
            }           

            return lstItens;
        }
    }

    public class XMLItens
    {
       public string operation { get; set; } = null;
       public string newValue { get; set; } = null;
       public string oldValue { get; set; } = null;
       public string tag { get; set; } = null;
    }
}
