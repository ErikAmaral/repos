﻿using LibCompareXML;
using Microsoft.XmlDiffPatch;
//using NetBike.XmlUnit;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
//using XmlDiffLib;

namespace charlesXML
{
    public partial class frmPrincipal : MetroFramework.Forms.MetroForm
    {
        string _arquivo1 = null;
        string _arquivo2 = null;
        string _opcao = null;


        public frmPrincipal()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Biblioteca para encontrar as diferenças entre dois arquivos XML.
        /// https://github.com/BrutalSimplicity/XmlDiffLib
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            Limpa();

            var exampleA = File.ReadAllText(_arquivo1);
            var exampleB = File.ReadAllText(_arquivo2);

            webBrowser1.Visible = false;

            var diff = new XmlDiffLib.XmlDiff(exampleA, exampleB);

            //diff.CompareDocuments(new XmlDiffOptions());
            //diff.ToString();

            var ttt = diff.CompareDocuments(new XmlDiffLib.XmlDiffOptions()
            {                
                IgnoreNamespace=true,
                IgnoreAttributeOrder = true,
                MatchValueTypes = true,
             //   MatchDescendants = false,
                TwoWayMatch = true
            }).ToString();

            txtProcessando.Text += string.Format("{0}", DateTime.Now.ToString("HH:mm:ss")) + Environment.NewLine;
            txtProcessando.Text += string.Format("Status: {0} ", ttt) + Environment.NewLine; ;
            txtProcessando.Text += string.Format("Comparação: {0} ", diff.ToString()) + Environment.NewLine;
            txtProcessando.SelectionStart = txtProcessando.Text.Length;
            txtProcessando.ScrollToCaret();

            this.Cursor = Cursors.Default;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            Limpa();

            string originalFile = _arquivo1;
            string finalFile = _arquivo2;
            string saida = @"C:\Users\erik.amaral\Desktop\Comapração\Saida.xml";

            XmlTextWriter diffGramWriter = new XmlTextWriter(saida,Encoding.UTF8);
            GenerateDiffGram1(originalFile, finalFile, diffGramWriter);

            webBrowser1.Visible = true;
            webBrowser1.Navigate(saida);

            this.Cursor = Cursors.Default;
        }

        /// <summary>
        /// Usando a ferramenta XML Diff and Patch em seus aplicativos
        /// @https://docs.microsoft.com/en-us/previous-versions/dotnet/articles/aa302294(v=msdn.10)?redirectedfrom=MSDN
        /// </summary>
        /// <param name="originalFile"></param>
        /// <param name="finalFile"></param>
        /// <param name="diffGramWriter"></param>
        public void GenerateDiffGram1(string originalFile, string finalFile,
                                    XmlWriter diffGramWriter)
        {
            //XmlDiff xmldiff = new XmlDiff(XmlDiffOptions.IgnoreChildOrder |
            //                                 XmlDiffOptions.IgnoreNamespaces |
            //                                 XmlDiffOptions.IgnorePrefixes);

            XmlDiff xmldiff = new XmlDiff();
            bool bIdentical = xmldiff.Compare(originalFile, finalFile, false, diffGramWriter);
            diffGramWriter.Close();
        }

        /// <summary>
        /// Usando a ferramenta XML Diff and Patch em seus aplicativos
        /// @https://docs.microsoft.com/en-us/previous-versions/dotnet/articles/aa302294(v=msdn.10)?redirectedfrom=MSDN
        /// </summary>
        /// <param name="originalFile"></param>
        /// <param name="finalFile"></param>
        /// <param name="diffGramWriter"></param>
        public void GenerateDiffGram2(string originalFile, string finalFile)
        {
            XmlDiff xmldiff = new XmlDiff(XmlDiffOptions.IgnoreChildOrder |
                                             XmlDiffOptions.IgnoreNamespaces |
                                             XmlDiffOptions.IgnorePrefixes);
            bool bIdentical = xmldiff.Compare(originalFile, finalFile, false);
            //diffGramWriter.Close();
        }


        /// <summary>
        /// https://github.com/netbike/netbike.xmlunit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            Limpa();
            //TextReader originalFile = new StreamReader(@"C:\Users\erik.amaral\Desktop\Comapração\ArquivosOriginA\names-db.xml");
            //TextReader finalFile = new StreamReader(@"C:\Users\erik.amaral\Desktop\Comapração\ArquivosOriginB\names-db.xml");

            TextReader originalFile = new StreamReader(_arquivo1);
            TextReader finalFile = new StreamReader(_arquivo2);

            webBrowser1.Visible = false;
            //NetBike.XmlUnit.XmlComparer comparer = new NetBike.XmlUnit.XmlComparer();

            //    var comparer = new NetBike.XmlUnit.XmlComparer
            //    {
            //        NormalizeText = true,
            //        Analyzer = NetBike.XmlUnit.XmlAnalyzer.Custom()
            //.SetEqual(NetBike.XmlUnit.XmlComparisonType.NodeListSequence)
            //.SetSimilar(NetBike.XmlUnit.XmlComparisonType.NamespacePrefix),
            //        Handler = NetBike.XmlUnit.XmlCompareHandling.Limit(10)
            //    };

            var comparer = new NetBike.XmlUnit.XmlComparer
            {
                NormalizeText = true,
                Analyzer = NetBike.XmlUnit.XmlAnalyzer.Custom()
                            .SetEqual(NetBike.XmlUnit.XmlComparisonType.NodeListSequence)
                            .SetSimilar(NetBike.XmlUnit.XmlComparisonType.NamespacePrefix),
                //Handler = NetBike.XmlUnit.XmlCompareHandling.Limit(10)
            };



            var result = comparer.Compare(originalFile, finalFile);


            if (!result.IsEqual)
            {
                foreach (var item in result.Differences)
                {
                    txtProcessando.Text +="===================>" + Environment.NewLine;
                    txtProcessando.Text += string.Format("{0}", DateTime.Now.ToString("HH:mm:ss")) + Environment.NewLine;
                    txtProcessando.Text += string.Format("Status: {0} ", item.State) + Environment.NewLine; ;
                    txtProcessando.Text += string.Format("Comparação: {0} ", item.Difference.ToString()) + Environment.NewLine;
                    txtProcessando.SelectionStart = txtProcessando.Text.Length;
                    txtProcessando.ScrollToCaret();                    
                }
            }
            this.Cursor = Cursors.Default;

        }

        private void button4_Click(object sender, EventArgs e)
        {
            openFileDialog.Title = "Abrir XML";
            openFileDialog.RestoreDirectory = true;
            openFileDialog.Filter = "Arquivo XML (*.xml)|*.xml";
            openFileDialog.FileName = "";            
            openFileDialog.ShowDialog();

            if (!string.IsNullOrWhiteSpace(openFileDialog.FileName))
            {
                _arquivo1 = openFileDialog.FileName;
                txtArquivo1.Text = _arquivo1;
            }     
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Limpa();
            openFileDialog2.Title = "Abrir XML";
            openFileDialog2.RestoreDirectory = true;
            openFileDialog2.Filter = "Arquivo XML (*.xml)|*.xml";
            openFileDialog2.FileName = "";
            openFileDialog2.ShowDialog();

            if (!string.IsNullOrWhiteSpace(openFileDialog2.FileName))
            {
                _arquivo2 = openFileDialog2.FileName;
                txtArquivo2.Text = _arquivo2;
                HabilitaObjetos();
            }
            else            
                DesabilitaObjetos();
            
        }

        private void fileSystemWatcher1_Changed(object sender, FileSystemEventArgs e)
        {

        }

        private void Limpa()
        {
            txtProcessando.Text = "";
            //webBrowser1.Dispose();
        }


        private void ExecutaProcessAssemblyDirectory2(string linhaParametros)
        {


            string mdcXml = AssemblyDirectory + "\\mdcxml.exe;";
            var originalPath = Environment.GetEnvironmentVariable("PATH");

            if (!originalPath.Contains(mdcXml))
                Environment.SetEnvironmentVariable("PATH", mdcXml + originalPath);

            System.Diagnostics.ProcessStartInfo proc = new System.Diagnostics.ProcessStartInfo();
            proc.FileName = @"C:\windows\system32\cmd.exe";
            proc.Arguments = "/c mdcxml.exe " + linhaParametros;
            proc.ErrorDialog = true;
            proc.UseShellExecute = false;
            proc.WindowStyle = ProcessWindowStyle.Maximized;

            try
            {
                using (Process exeProcess = Process.Start(proc))
                {
                    exeProcess.WaitForExit();
                }
            }
            catch (Exception erro)
            {
                // Log error.
            }
        }

        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;  

            if (string.IsNullOrWhiteSpace(_opcao))
                _opcao = "D";
          
            string originalFile = _arquivo1;
            string finalFile = _arquivo2;
            string saida = @"C:\Users\erik.amaral\Desktop\Comapração\Resultado.xml";
            string linhaParametros = string.Format("{0} {1} {2} /{3} options", originalFile, finalFile, saida, _opcao);
             ExecutaProcessAssemblyDirectory2(linhaParametros);
            webBrowser1.Visible = true;
            webBrowser1.Navigate(saida);

            this.Cursor = Cursors.Default;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            string arquivo1 = null;
            string arquivo2 = null;

            arquivo1 = _arquivo1;
            arquivo2 = _arquivo2;

            txtArquivo1.Text = "";
            txtArquivo2.Text = "";

            txtArquivo1.Text = arquivo2;
            txtArquivo2.Text = arquivo1;

            _arquivo1 = txtArquivo1.Text;
            _arquivo2 = txtArquivo2.Text;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            frmOpcoes opcoes = new frmOpcoes(_opcao);
            opcoes.ShowDialog();
            _opcao = opcoes.opcaoSelecionado;
        }

        private void HabilitaObjetos()
        {
            button3.Enabled = true;
            button1.Enabled = true;
            button2.Enabled = true;
            button6.Enabled = true;
            button7.Enabled = true;
        }


        private void DesabilitaObjetos()
        {
            button3.Enabled = false;
            button1.Enabled = false;
            button2.Enabled = false;
            button6.Enabled = false;
            button7.Enabled = false;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            List<XMLItens> lstItens = new List<XMLItens>();
            string saida = @"C:\Users\erik.amaral\Desktop\Comapração\Resultado.xml";
            LibCompareXML.XMLExtract xLoad = new LibCompareXML.XMLExtract(saida);
            lstItens= xLoad.LoadCompare();

            frmResumo resumo = new frmResumo(lstItens);
            resumo.ShowDialog();
        }
    }
}
