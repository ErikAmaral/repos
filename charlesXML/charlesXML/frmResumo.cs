﻿using LibCompareXML;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace charlesXML
{
    public partial class frmResumo : MetroFramework.Forms.MetroForm
    {
        List<XMLItens> _lstItens = new List<XMLItens>();

        public frmResumo()
        {
            InitializeComponent();
        }

        public frmResumo(List<XMLItens> lstItens)
        {
            InitializeComponent();
            _lstItens = lstItens;
            CarregaQuantitativo();
            CarregaQuantitativoListi();
        }

        private void CarregaQuantitativo()
        {
            lblAdicionados.Text = _lstItens.Where(a => a.operation.Equals("start_elements_added")).Count().ToString();
            lblAlterados.Text = _lstItens.Where(a => a.operation.Equals("start_value_changed")).Count().ToString();
            lblRemovidos.Text = _lstItens.Where(a => a.operation.Equals("start_elements_deleted")).Count().ToString();
        }

        private void CarregaQuantitativoListi()
        {
            var elementosAdicionados = _lstItens.Where(a => a.operation.Equals("start_elements_added")).Select(b => b.tag);
            foreach (var item in elementosAdicionados)
            {
                ListViewItem status = new ListViewItem(item);                
                status.SubItems.Add(item);
                lstAdicionados.Items.AddRange(new ListViewItem[] { status });
            }

            //var elementosAlterados = _lstItens.Where(a => a.operation.Equals("start_value_changed")).Select(b => b.newValue);

            var elementosAlterados = _lstItens.Where(a => a.operation.Equals("start_value_changed")).Select(s => new XMLItens { newValue = s.newValue, oldValue = s.oldValue });
            foreach (var item in elementosAlterados)
            {
                ListViewItem status = new ListViewItem(item.oldValue);
                status.SubItems.Add(item.newValue);
                //status.SubItems.Add(" ");
                lstAlterados.Items.AddRange(new ListViewItem[] { status });
            }

            var elementosRemovidos = _lstItens.Where(a => a.operation.Equals("start_elements_deleted")).Select(b => b.tag);
            foreach (var item in elementosRemovidos)
            {
                ListViewItem status = new ListViewItem(item);
                status.SubItems.Add(item);
                lstRemovidos.Items.AddRange(new ListViewItem[] { status });
            }
        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();          
        }
    }
}
