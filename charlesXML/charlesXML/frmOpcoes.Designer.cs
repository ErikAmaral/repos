﻿namespace charlesXML
{
    partial class frmOpcoes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpMCDXML = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.rdbM = new System.Windows.Forms.RadioButton();
            this.rdbD = new System.Windows.Forms.RadioButton();
            this.rdbR = new System.Windows.Forms.RadioButton();
            this.rdbE = new System.Windows.Forms.RadioButton();
            this.rdbS = new System.Windows.Forms.RadioButton();
            this.rdbC = new System.Windows.Forms.RadioButton();
            this.rdbB = new System.Windows.Forms.RadioButton();
            this.grpMCDXML.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpMCDXML
            // 
            this.grpMCDXML.Controls.Add(this.rdbB);
            this.grpMCDXML.Controls.Add(this.rdbE);
            this.grpMCDXML.Controls.Add(this.rdbS);
            this.grpMCDXML.Controls.Add(this.rdbC);
            this.grpMCDXML.Controls.Add(this.rdbR);
            this.grpMCDXML.Controls.Add(this.rdbD);
            this.grpMCDXML.Controls.Add(this.rdbM);
            this.grpMCDXML.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpMCDXML.Location = new System.Drawing.Point(3, 4);
            this.grpMCDXML.Name = "grpMCDXML";
            this.grpMCDXML.Size = new System.Drawing.Size(412, 196);
            this.grpMCDXML.TabIndex = 0;
            this.grpMCDXML.TabStop = false;
            this.grpMCDXML.Text = "Opções";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(140, 206);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(124, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // rdbM
            // 
            this.rdbM.AutoSize = true;
            this.rdbM.Location = new System.Drawing.Point(17, 26);
            this.rdbM.Name = "rdbM";
            this.rdbM.Size = new System.Drawing.Size(220, 19);
            this.rdbM.TabIndex = 0;
            this.rdbM.TabStop = true;
            this.rdbM.Text = "M Mesclar dois arquivos XML (padrão)";
            this.rdbM.UseVisualStyleBackColor = true;
            // 
            // rdbD
            // 
            this.rdbD.AutoSize = true;
            this.rdbD.Location = new System.Drawing.Point(17, 48);
            this.rdbD.Name = "rdbD";
            this.rdbD.Size = new System.Drawing.Size(245, 19);
            this.rdbD.TabIndex = 1;
            this.rdbD.TabStop = true;
            this.rdbD.Text = "D Salvar diferenças entre dois arquivos XML";
            this.rdbD.UseVisualStyleBackColor = true;
            // 
            // rdbR
            // 
            this.rdbR.AutoSize = true;
            this.rdbR.Location = new System.Drawing.Point(17, 72);
            this.rdbR.Name = "rdbR";
            this.rdbR.Size = new System.Drawing.Size(178, 19);
            this.rdbR.TabIndex = 2;
            this.rdbR.TabStop = true;
            this.rdbR.Text = "R Salvar relatório de diferenças";
            this.rdbR.UseVisualStyleBackColor = true;
            // 
            // rdbE
            // 
            this.rdbE.AutoSize = true;
            this.rdbE.Location = new System.Drawing.Point(17, 141);
            this.rdbE.Name = "rdbE";
            this.rdbE.Size = new System.Drawing.Size(362, 19);
            this.rdbE.TabIndex = 5;
            this.rdbE.TabStop = true;
            this.rdbE.Text = "E Salvar apenas elementos XML excluídos do primeiro arquivo XML";
            this.rdbE.UseVisualStyleBackColor = true;
            // 
            // rdbS
            // 
            this.rdbS.AutoSize = true;
            this.rdbS.Location = new System.Drawing.Point(17, 117);
            this.rdbS.Name = "rdbS";
            this.rdbS.Size = new System.Drawing.Size(353, 19);
            this.rdbS.TabIndex = 4;
            this.rdbS.TabStop = true;
            this.rdbS.Text = "Salvar apenas elementos XML diferentes do primeiro arquivo XML";
            this.rdbS.UseVisualStyleBackColor = true;
            // 
            // rdbC
            // 
            this.rdbC.AutoSize = true;
            this.rdbC.Location = new System.Drawing.Point(17, 95);
            this.rdbC.Name = "rdbC";
            this.rdbC.Size = new System.Drawing.Size(313, 19);
            this.rdbC.TabIndex = 3;
            this.rdbC.TabStop = true;
            this.rdbC.Text = "C Salva elementos XML comuns do primeiro arquivo XML";
            this.rdbC.UseVisualStyleBackColor = true;
            // 
            // rdbB
            // 
            this.rdbB.AutoSize = true;
            this.rdbB.Location = new System.Drawing.Point(17, 163);
            this.rdbB.Name = "rdbB";
            this.rdbB.Size = new System.Drawing.Size(379, 19);
            this.rdbB.TabIndex = 6;
            this.rdbB.TabStop = true;
            this.rdbB.Text = "B Salva elementos XML excluídos e diferentes do primeiro arquivo XML";
            this.rdbB.UseVisualStyleBackColor = true;
            // 
            // frmOpcoes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(417, 238);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.grpMCDXML);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmOpcoes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.grpMCDXML.ResumeLayout(false);
            this.grpMCDXML.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpMCDXML;
        private System.Windows.Forms.RadioButton rdbB;
        private System.Windows.Forms.RadioButton rdbE;
        private System.Windows.Forms.RadioButton rdbS;
        private System.Windows.Forms.RadioButton rdbC;
        private System.Windows.Forms.RadioButton rdbR;
        private System.Windows.Forms.RadioButton rdbD;
        private System.Windows.Forms.RadioButton rdbM;
        private System.Windows.Forms.Button button1;
    }
}