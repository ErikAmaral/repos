﻿namespace charlesXML
{
    partial class frmResumo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.exportarPDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sairToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lstAlterados = new System.Windows.Forms.ListView();
            this.Antigo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Novo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblAlterados = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lstRemovidos = new System.Windows.Forms.ListView();
            this.Removidos = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblRemovidos = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lstAdicionados = new System.Windows.Forms.ListView();
            this.Adicionado = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblAdicionados = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.menuStrip1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportarPDFToolStripMenuItem,
            this.exportarToolStripMenuItem,
            this.sairToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(20, 60);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(682, 55);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // exportarPDFToolStripMenuItem
            // 
            this.exportarPDFToolStripMenuItem.Image = global::charlesXML.Properties.Resources.pdfs;
            this.exportarPDFToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.exportarPDFToolStripMenuItem.Name = "exportarPDFToolStripMenuItem";
            this.exportarPDFToolStripMenuItem.Size = new System.Drawing.Size(86, 51);
            this.exportarPDFToolStripMenuItem.Text = "Exportar PDF";
            this.exportarPDFToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // exportarToolStripMenuItem
            // 
            this.exportarToolStripMenuItem.Image = global::charlesXML.Properties.Resources.xls_64274;
            this.exportarToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.exportarToolStripMenuItem.Name = "exportarToolStripMenuItem";
            this.exportarToolStripMenuItem.Size = new System.Drawing.Size(87, 51);
            this.exportarToolStripMenuItem.Text = " Exportar XLS";
            this.exportarToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // sairToolStripMenuItem
            // 
            this.sairToolStripMenuItem.Image = global::charlesXML.Properties.Resources.power_32;
            this.sairToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.sairToolStripMenuItem.Name = "sairToolStripMenuItem";
            this.sairToolStripMenuItem.Size = new System.Drawing.Size(44, 51);
            this.sairToolStripMenuItem.Text = "Sair";
            this.sairToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.sairToolStripMenuItem.Click += new System.EventHandler(this.sairToolStripMenuItem_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lstAlterados);
            this.groupBox3.Controls.Add(this.lblAlterados);
            this.groupBox3.Controls.Add(this.metroLabel1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(20, 115);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(682, 286);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Valores Alterados";
            // 
            // lstAlterados
            // 
            this.lstAlterados.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstAlterados.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Antigo,
            this.Novo});
            this.lstAlterados.Location = new System.Drawing.Point(7, 59);
            this.lstAlterados.Name = "lstAlterados";
            this.lstAlterados.Size = new System.Drawing.Size(669, 221);
            this.lstAlterados.TabIndex = 2;
            this.lstAlterados.UseCompatibleStateImageBehavior = false;
            this.lstAlterados.View = System.Windows.Forms.View.Details;
            // 
            // Antigo
            // 
            this.Antigo.Text = "Antigo Valor";
            this.Antigo.Width = 310;
            // 
            // Novo
            // 
            this.Novo.Text = "Novo Valor";
            this.Novo.Width = 420;
            // 
            // lblAlterados
            // 
            this.lblAlterados.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAlterados.AutoSize = true;
            this.lblAlterados.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblAlterados.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblAlterados.Location = new System.Drawing.Point(312, 13);
            this.lblAlterados.Name = "lblAlterados";
            this.lblAlterados.Size = new System.Drawing.Size(22, 25);
            this.lblAlterados.Style = MetroFramework.MetroColorStyle.Orange;
            this.lblAlterados.TabIndex = 1;
            this.lblAlterados.Text = "0";
            this.lblAlterados.UseStyleColors = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(273, 31);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(108, 25);
            this.metroLabel1.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Alterado(s)";
            this.metroLabel1.UseStyleColors = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Location = new System.Drawing.Point(20, 410);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(682, 323);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.lstRemovidos);
            this.groupBox5.Controls.Add(this.lblRemovidos);
            this.groupBox5.Controls.Add(this.metroLabel3);
            this.groupBox5.Location = new System.Drawing.Point(341, 20);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(335, 297);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Elementos Removidos";
            // 
            // lstRemovidos
            // 
            this.lstRemovidos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstRemovidos.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Removidos});
            this.lstRemovidos.Location = new System.Drawing.Point(8, 60);
            this.lstRemovidos.Name = "lstRemovidos";
            this.lstRemovidos.Size = new System.Drawing.Size(321, 231);
            this.lstRemovidos.TabIndex = 4;
            this.lstRemovidos.UseCompatibleStateImageBehavior = false;
            this.lstRemovidos.View = System.Windows.Forms.View.Details;
            // 
            // Removidos
            // 
            this.Removidos.Text = "Removidos";
            this.Removidos.Width = 320;
            // 
            // lblRemovidos
            // 
            this.lblRemovidos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRemovidos.AutoSize = true;
            this.lblRemovidos.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblRemovidos.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblRemovidos.Location = new System.Drawing.Point(160, 15);
            this.lblRemovidos.Name = "lblRemovidos";
            this.lblRemovidos.Size = new System.Drawing.Size(22, 25);
            this.lblRemovidos.Style = MetroFramework.MetroColorStyle.Orange;
            this.lblRemovidos.TabIndex = 3;
            this.lblRemovidos.Text = "0";
            this.lblRemovidos.UseStyleColors = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel3.Location = new System.Drawing.Point(118, 32);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(120, 25);
            this.metroLabel3.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroLabel3.TabIndex = 2;
            this.metroLabel3.Text = "Removido(s)";
            this.metroLabel3.UseStyleColors = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.lstAdicionados);
            this.groupBox4.Controls.Add(this.lblAdicionados);
            this.groupBox4.Controls.Add(this.metroLabel2);
            this.groupBox4.Location = new System.Drawing.Point(3, 20);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(332, 297);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Elementos Adicionados";
            // 
            // lstAdicionados
            // 
            this.lstAdicionados.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstAdicionados.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Adicionado});
            this.lstAdicionados.Location = new System.Drawing.Point(6, 60);
            this.lstAdicionados.Name = "lstAdicionados";
            this.lstAdicionados.Size = new System.Drawing.Size(320, 231);
            this.lstAdicionados.TabIndex = 3;
            this.lstAdicionados.UseCompatibleStateImageBehavior = false;
            this.lstAdicionados.View = System.Windows.Forms.View.Details;
            // 
            // Adicionado
            // 
            this.Adicionado.Text = "Adicionado";
            this.Adicionado.Width = 308;
            // 
            // lblAdicionados
            // 
            this.lblAdicionados.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAdicionados.AutoSize = true;
            this.lblAdicionados.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblAdicionados.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblAdicionados.Location = new System.Drawing.Point(140, 15);
            this.lblAdicionados.Name = "lblAdicionados";
            this.lblAdicionados.Size = new System.Drawing.Size(22, 25);
            this.lblAdicionados.Style = MetroFramework.MetroColorStyle.Orange;
            this.lblAdicionados.TabIndex = 2;
            this.lblAdicionados.Text = "0";
            this.lblAdicionados.UseStyleColors = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel2.Location = new System.Drawing.Point(92, 32);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(131, 25);
            this.metroLabel2.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroLabel2.TabIndex = 1;
            this.metroLabel2.Text = "Adicionado(s)";
            this.metroLabel2.UseStyleColors = true;
            // 
            // frmResumo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(722, 753);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "frmResumo";
            this.Text = "Resumo da Comparação";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem exportarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportarPDFToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel lblAlterados;
        private System.Windows.Forms.ColumnHeader Novo;
        private System.Windows.Forms.ColumnHeader Antigo;
        private System.Windows.Forms.ListView lstAlterados;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel lblAdicionados;
        private System.Windows.Forms.ColumnHeader Adicionado;
        private System.Windows.Forms.ListView lstAdicionados;
        private System.Windows.Forms.GroupBox groupBox4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel lblRemovidos;
        private System.Windows.Forms.ColumnHeader Removidos;
        private System.Windows.Forms.ListView lstRemovidos;
        private System.Windows.Forms.GroupBox groupBox5;
    }
}